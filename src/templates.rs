use std::collections::HashMap;

use tera::{Tera, Value, Result as TeraResult};

fn mes(s: &Value, _context: &HashMap<String, Value>) -> TeraResult<Value> {
    Ok(match s {
        Value::String(s) => match s.as_str() {
            "01" => Value::String(String::from("enero")),
            "02" => Value::String(String::from("febrero")),
            "03" => Value::String(String::from("marzo")),
            "04" => Value::String(String::from("abril")),
            "05" => Value::String(String::from("mayo")),
            "06" => Value::String(String::from("junio")),
            "07" => Value::String(String::from("julio")),
            "08" => Value::String(String::from("agosto")),
            "09" => Value::String(String::from("septiembre")),
            "10" => Value::String(String::from("octubre")),
            "11" => Value::String(String::from("noviembre")),
            "12" => Value::String(String::from("diciembre")),
            _ => Value::String(String::from("--")),
        },
        _ => Value::String(String::from("--")),
    })
}

pub fn compile() -> Tera {
    let mut tera = match Tera::new("templates/**/*") {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };

    tera.autoescape_on(vec![".html"]);
    tera.register_filter("mes", mes);

    tera
}
