use std::path::PathBuf;
use std::fs::{File, read_dir};

use clap::Parser;
use claxon::{FlacReader, metadata::StreamInfo};
use sha1::{Sha1, Digest};

use sonido::web;
use sonido::settings::Settings;
use sonido::models::{Sound, SoundCollection};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Cli {
    /// Directory where audio files are stored
    #[arg(value_name = "DIR")]
    sound_dir: PathBuf,

    /// How many bars will the histogram have (more bars need more memory)
    #[arg(short, long, value_name="NUM", default_value_t=50)]
    num_bars: usize,
}

fn get_tag<'a>(reader: &'a FlacReader<File>, tagname: &'a str) -> Option<&'a str> {
    let mut description = reader.get_tag(tagname);

    description.next()
}

fn main() {
    env_logger::builder()
        .format_timestamp(None)
        .parse_default_env()
        .init();

    let args = Cli::parse();
    let file_iter = read_dir(&args.sound_dir)
        .unwrap()
        .filter_map(|f| f.ok())
        .filter(|f| f.file_name().into_string().unwrap().ends_with(".flac"));

    let mut sc = SoundCollection::new();

    for file in file_iter {
        let reader = claxon::FlacReader::open(file.path()).unwrap();
        let emojis = get_tag(&reader, "DESCRIPTION").unwrap();
        let title = get_tag(&reader, "TITLE").unwrap();
        let date = get_tag(&reader, "DATE").unwrap();
        let place = get_tag(&reader, "PLACE").unwrap();

        println!("Processing {:?} [{}]", file.path(), &emojis);

        let file_name = file
            .path().file_stem().unwrap().to_string_lossy().into_owned();

        let mut hasher = Sha1::new();
        hasher.update(&file_name);
        let result = hasher.finalize();
        let emojis = emojis.to_owned();
        let title = title.to_owned();
        let date = date.parse().unwrap();
        let place = place.to_owned();

        let mut reader = reader;
        let num_bars = args.num_bars;
        let StreamInfo { channels, samples: num_samples, .. } = reader.streaminfo();
        let window_size = num_samples.unwrap() as usize/num_bars;
        let samples: Vec<_> = reader.samples().filter_map(|s| s.ok()).collect();
        let bars: Vec<_> = samples.chunks_exact(window_size * channels as usize).map(|c| {
            let (min, max) = c.iter().fold((0, 0), |(min, max), &s| {
                (
                    if s < min { s } else { min },
                    if s > max { s } else { max },
                )
            });

            (min as f64/32768.0 * 100.0, max as f64/32768.0 * 100.0)
        }).collect();

        let max_percentage = bars.iter().map(|(mi, ma)| (mi.abs() as u16).max(ma.abs() as u16)).max().unwrap();

        sc.add(Sound {
            title, file_name, emojis, date, place, bars, max_percentage,
            hash: hex::encode(&result[..]),
        });
    }

    println!("Total: {} sounds", sc.count());

    let settings = Settings::from_env();

    web::main(settings, args.sound_dir, sc).unwrap();
}
