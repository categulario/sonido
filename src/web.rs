use std::path::PathBuf;

use actix_web::{web, get, App, HttpResponse, HttpServer, Responder};
use actix_files::Files;
use tera::{Tera, Context};

use crate::settings::Settings;
use crate::templates;
use crate::models::SoundCollection;

#[get("/")]
async fn home(app_data: web::Data<AppData>) -> impl Responder {
    let mut context = Context::new();
    let (hash, _) = app_data.sc.random().unwrap();

    context.insert("sound_hash", hash);

    HttpResponse::Ok().body(
        app_data.tera.render("index.html", &context).unwrap()
    )
}

#[get("/sound/{hash}")]
async fn sound(app_data: web::Data<AppData>, params: web::Path<(String,)>) -> impl Responder {
    let hash = params.into_inner().0;
    let sound = app_data.sc.get(&hash);

    if let Some(sound) = sound {
        let mut context = Context::new();
        let more_sounds = vec![
            app_data.sc.random().unwrap().1,
            app_data.sc.random().unwrap().1,
        ];

        context.insert("sound", &sound);
        context.insert("more_sounds", &more_sounds);

        HttpResponse::Ok().body(
            app_data.tera.render("sound.html", &context).unwrap()
        )
    } else {
        HttpResponse::NotFound().body(
            app_data.tera.render("404.html", &Context::new()).unwrap()
        )
    }
}

struct AppData {
    tera: Tera,
    sc: SoundCollection,
}

#[actix_web::main]
pub async fn main(settings: Settings, sound_dir: PathBuf, sc: SoundCollection) -> std::io::Result<()> {
    let host = settings.host;
    let port = settings.port;

    log::info!("Will bind server to {host}:{port}");

    HttpServer::new(move || {
        let tera = templates::compile();
        let sc = sc.clone();
        let app_data = AppData {
            tera,
            sc,
        };

        App::new()
            .app_data(web::Data::new(app_data))
            .service(Files::new("/static", "static"))
            .service(Files::new("/audio", sound_dir.clone()))
            .service(sound)
            .service(home)
    })
    .bind((host, port))?
    .run()
    .await
}
