use std::env;

pub struct Settings {
    pub host: String,
    pub port: u16,
}

impl Settings {
    pub fn from_env() -> Settings {
        Settings {
            host: env::var("SONIDO_HOST").unwrap_or(Settings::default().host),
            port: env::var("SONIDO_PORT").ok().and_then(|x| x.parse().ok()).unwrap_or(Settings::default().port),
        }
    }
}

impl Default for Settings {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".into(),
            port: 8000,
        }
    }
}
