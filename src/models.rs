use std::collections::HashMap;
use std::str::FromStr;

use rand::prelude::*;
use serde::Serialize;

#[derive(Clone, Serialize)]
pub struct Date {
    pub year: u16,
    pub month: String,
    pub day: u8,
}

impl FromStr for Date {
    type Err = String;

    fn from_str(s: &str) -> Result<Date, Self::Err> {
        let parts: Vec<_> = s.split('-').collect();

        Ok(Date {
            year: parts.first().and_then(|p| p.parse().ok()).ok_or(String::from("invalid date format"))?,
            month: parts.get(1).map(|p| p.to_string()).ok_or(String::from("invalid date"))?,
            day: parts.get(2).and_then(|p| p.parse().ok()).ok_or(String::from("invalid date format"))?,
        })
    }
}

#[derive(Clone, Serialize)]
pub struct Sound {
    pub title: String,
    pub file_name: String,
    pub emojis: String,
    pub hash: String,
    pub date: Date,
    pub place: String,
    pub bars: Vec<(f64, f64)>,
    pub max_percentage: u16,
}

#[derive(Clone)]
pub struct SoundCollection {
    sounds: HashMap<String, Sound>,
}

impl SoundCollection {
    pub fn new() -> Self {
        Self {
            sounds: HashMap::new(),
        }
    }

    pub fn add(&mut self, sound: Sound) {
        self.sounds.insert(sound.hash.clone(), sound);
    }

    pub fn count(&self) -> usize {
        self.sounds.len()
    }

    pub fn get(&self, hash: &str) -> Option<&Sound> {
        self.sounds.get(hash)
    }

    pub fn random(&self) -> Option<(&String, &Sound)> {
        let mut rng = thread_rng();

        self.sounds.iter().choose(&mut rng)
    }
}

impl Default for SoundCollection {
    fn default() -> Self {
        Self::new()
    }
}
