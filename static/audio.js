const AUDIO_ELEMENT = document.getElementById('audio');
const PROGRESS_ELEMENT = document.getElementById('progress');
let is_playing = false;

function play(btn) {
  AUDIO_ELEMENT.play();
  is_playing = true;
  btn.classList.toggle('playing');
  btn.ariaLabel = "Detener reproducción"
}

function pause(btn) {
  AUDIO_ELEMENT.pause();
  is_playing = false;
  btn.classList.toggle('playing');
  btn.ariaLabel = "Reproducir sonido"
}

function toggle(btn) {
  if (!is_playing) {
    play(btn);
  } else {
    pause(btn);
  }
}

AUDIO_ELEMENT.addEventListener('ended', () => {
  pause(document.getElementById('togglebtn'));
  PROGRESS_ELEMENT.style.width = '0';
  PROGRESS_ELEMENT.ariaValueNow = '0';
});

AUDIO_ELEMENT.addEventListener('timeupdate', () => {
  const elapsedSecs = AUDIO_ELEMENT.currentTime;
  const progress = elapsedSecs / AUDIO_ELEMENT.duration * 100;

  PROGRESS_ELEMENT.style.width = `${progress}%`;
  PROGRESS_ELEMENT.ariaValueNow = progress.toString();
});
