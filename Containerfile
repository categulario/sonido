# Build sonido
FROM docker.io/rust:1.76-bookworm AS build

COPY ./Cargo.toml /
COPY ./Cargo.lock /
COPY ./src /src

RUN cargo build --locked --release

FROM debian:bookworm

ENV SONIDO_HOME=/opt/sonido
ENV SONIDO_HOST=0.0.0.0

RUN useradd --system --create-home --user-group \
    --uid 900 --home-dir $SONIDO_HOME \
    --shell /bin/false \
    sonido

USER sonido
WORKDIR $SONIDO_HOME

COPY --from=build /target/release/sonido /opt/sonido/sonido
COPY ./templates /opt/sonido/templates
COPY ./static /opt/sonido/static

EXPOSE 8000

ENTRYPOINT ["/opt/sonido/sonido"]

CMD ["/var/lib/sonidos"]
