#!/usr/bin/fish

for name in (eza *.flac)
    set -l parts (string split '.' $name)
    set -l basename $parts[1]

    if not test -e $basename.m4a
        ffmpeg -i $basename.flac -c:a libfdk_aac -b:a 128k $basename.m4a
    end
end
