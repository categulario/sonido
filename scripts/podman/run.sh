#!/bin/bash

__dir=$(dirname $(realpath $0))

container=sonido
image=registry.gitlab.com/categulario/sonido/sonido

if [[ -z $@ ]]; then
    cmd=$image
    name="--name=$container"

    podman rm -i $container
else
    cmd="-it $image $@"
fi

podman run --rm \
    --uidmap 900:0:1 --uidmap 0:1:900 \
    --volume $__dir/../../resources/sounds:/var/lib/sonidos:U \
    --publish 8000:8000 \
    --env RUST_LOG=info \
    $name \
    $cmd
